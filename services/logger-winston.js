const {createLogger, format} = require('winston');
const winston = require('winston');

const rootFolder = __dirname + '/../logs';

const options = {
    fileAll: {
        level: 'debug',
        filename: `${rootFolder}/all.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    fileError: {
        level: 'error',
        filename: `${rootFolder}/error.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

const logger = createLogger({
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(info => [info.timestamp.trim(), info.level.toUpperCase(), info.message].join("\t"))
    ),
    transports: [
        // - Write to all logs with level `debug` and below to `all.log`
        // - Write all logs error (and below) to `error.log`.
        new winston.transports.File(options.fileAll),
        new winston.transports.File(options.fileError),
        new winston.transports.Console(options.console)
    ],
    exitOnError: false // do not exit on handled exceptions
});


logger.stream = {
    write: function (message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};

module.exports = logger;