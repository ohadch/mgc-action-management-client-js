const Joi = require('joi');
const { RequestValidation } = require('request-validation');

// Create validation for routes as Joi schemas
module.exports = new RequestValidation({
    signUp: {
        body: Joi.object().keys({
            email: Joi.string().email({ minDomainAtoms: 2 }).required(),
            first_name: Joi.string().alphanum().required(),
            last_name: Joi.string().alphanum().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
            confirm_password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
        })
    },

    login: {
        body: Joi.object().keys({
            email: Joi.string().email({ minDomainAtoms: 2 }).required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        })
    },
});

