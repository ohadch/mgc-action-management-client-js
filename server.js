"use strict";
require('dotenv').config();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const expressWinston = require('express-winston');
const errorHandler = require('errorhandler');
const exphbs  = require('express-handlebars');
const favicon = require('express-favicon');
const fs = require("fs");
const flash = require('connect-flash');
const winston = require('winston');
const cors = require('cors');

const PORT = process.env.PORT || 8000;
const isProduction = process.env.NODE_ENV === 'production';

// Initiate the app
const app = express();

// Initialize the handlebars engine
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Authentication Packages
const session = require('express-session');
const passport = require('passport');

// Create logs folder
const logsDirPath = `${__dirname}/logs`;
if (!fs.existsSync(logsDirPath)) {
    fs.mkdirSync(logsDirPath);
}

// Log requests to console
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: false, // optional: control whether you want to log the meta data about the request (default to true)
    msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
    expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
    colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
    ignoreRoute: function (req, res) {
        return false;
    } // optional: allows to skip some log messages based on request and/or response
}));

// Configure the app
app.set('superSecret', 'vario');
app.use(favicon(__dirname + '/public/favicon.jpg'));
app.use(cors());
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/vue/dist'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
    secret: 'duo discus',
    resave: false,
    saveUninitialized: false,
    // cookie: { secure: true }, // true when https
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Routes
app.use('/rest/', require('./app/routes/api-routes'));
app.use(require('./app/routes/app-routes'));

require('./app/passport');

if (!isProduction) {
    app.use(errorHandler());
}

app.listen(PORT, function() {
    console.log("==> ?  Listening on port %s. Visit http://localhost:%s/ in your browser.", PORT, PORT);
});