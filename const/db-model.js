const tables = [
    {
        name: 'GeneralSetting',
        table: 't08GeneralSetting',
        constraints: {
            pk: 'SettingPKCode'
        },
        fields: {
            SettingPKCode: 'integer',
            SettingName: 'string',
            SettingDate: 'date'
        }
    },

    {
        name: 'Action',
        table: 't04Actions',
        constraints: {
            pk: 'ActionPKCode'
        },
        fields: {
            ActionPKCode: 'integer',
            ActionDate: 'date'
        }
    },
    {
        name: 'Duty',
        table: 'st04DutyPersonsOnAction',
        constraints: {
            pk: 'DutyPKCode',
            fks: {
                action: {
                    field: 'ActionFKCode',
                    model: 'Action'
                },
                member: {
                    field: 'PersonNameIDSC',
                    model: 'Member'
                },
                job: {
                    field: 'JobID',
                    model: 'Job'
                }
            }
        },
        fields: {
            ActionFKCode: 'integer',
            PersonNameIDSC: 'integer',
            JobID: 'integer',
            JobTypeID: 'integer',
            HoursQty: 'integer',
            Payment: 'integer',
            Note: 'string'
        }
    },

    {
        name: 'Flight',
        table: 'st04ActionsDetails',
        constraints: {
            pk: 'FlightPKcode',
            fks: {
                flightType: {
                    field: 'FlightTypeIDSC',
                    model: 'FlightType'
                },
                pilot1: {
                    field: 'Pilot1NameIDSC',
                    model: 'Member'
                },
                pilot2: {
                    field: 'Pilot2NameIDSC',
                    model: 'Member'
                },
                pilot1Type: {
                    field: 'Pilot1TypeIDSC',
                    model: 'Status'
                },
                pilot2Type: {
                    field: 'Pilot2TypeIDSC',
                    model: 'Status'
                },
                dragType: {
                    field: 'DragTypeIDSC',
                    model: 'TowType'
                },
                dragPilot: {
                    field: 'DragPilotIDSC',
                    model: 'Member'
                },
                flightCharacter: {
                    field: 'FlightCharacterID',
                    model: 'FlightCharacter'
                },
                action: {
                    field: 'ActionFKCode',
                    model: 'Action'
                },
                glider: {
                    field: 'RadioCallIDSC',
                    model: 'Aircraft'
                },
                payersType: {
                    field: 'PayersTypeIDSC',
                    model: 'PayersTypes'
                },
                payer: {
                    field: 'PayerNameIDSC',
                    model: 'Member'
                },
                dragPlane: {
                    field: 'DragPlaneRadioCallIDSC',
                    model: 'Aircraft'
                },
                paymentType: {
                    field: 'PaymentTypeID',
                    model: 'PaymentType'
                },
                paymentReceiver: {
                    field: 'RecivePayNameIDSC',
                    model: 'Member'
                }
            }
        },
        fields: {
            TakeOffTime: 'datetime',
            LandTime: 'datetime',
            FlightTypeIDSC: 'integer',
            Pilot1NameIDSC: 'integer',
            Pilot2NameIDSC: 'integer',
            Pilot1TypeIDSC: 'integer',
            Pilot2TypeIDSC: 'integer',
            DragTypeIDSC: 'integer',
            DragHeight: 'integer',
            DragPilotIDSC: 'integer',
            TakeOffCost: 'integer',
            StayCost: 'float',
            StayTime: 'integer',
            OtherCost: 'integer',
            SumCost: 'integer',
            SumPayPilot1: 'float',
            SumPayPilot2: 'float',
            FlightCharacterID: 'integer',
            FlightDistance: 'integer',
            MaxHeight: 'integer',
            MissionTime: 'integer',
            AvrgMissionSpeed: 'integer',
            DragTime: 'integer',
            ActionFKCode: 'integer',
            RadioCallIDSC: 'integer',
            FlightNumber: 'integer',
            PayersTypeIDSC: 'integer',
            PayerNameIDSC: 'integer',
            SumPayOtherPayer: 'integer',
            FlightPKcode: 'integer',
            DragPlaneRadioCallIDSC: 'integer',
            PaymentTypeID: 'integer',
            RecivePayNameIDSC: 'integer'
        }
    },
    {
        name: 'FlightType',
        table: 'st08FlightTypes',
        constraints: {
            pk: 'FlightTypeCode',
            fks: {
                setting: {
                    field: 'SettingFKCode',
                    model: 'GeneralSetting'
                }
            }
        },
        fields: {
            FlightTypeCode: 'integer',
            FlightType: 'string',
            FlightTypeParam: 'string',
            SettingFKCode: 'integer'
        }
    },
    {
        name: 'FlightCharacter',
        table: 'ctFlightCharacterID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'TowType',
        table: 'st08TowingTypes',
        constraints: {
            pk: 'TowingTypeCode',
            fks: {
                generalSetting: {
                    field: 'SettingFKCode',
                    model: 'GeneralSetting'
                }
            }
        },
        fields: {
            TowingTypeCode: 'integer',
            TowingType: 'string',
            TowingTypeParam: 'string',
            SettingFKCode: 'integer'
        }
    },

    {
        name: 'Aircraft',
        table: 't03Glider',
        constraints: {
            pk: 'GPKCode',
            fks: {
                producer: {
                    field: 'ProducerID',
                    model: 'AircraftProducer'
                },
                model: {
                    field: 'ModelID',
                    model: 'AircraftModel'
                }
            }
        },
        fields: {
            GPKCode: 'integer',
            Gname: 'string',
            RadioCall: 'string',
            ModelID: 'integer',
            LicensesNum: 'string',
            ProductionDate: 'date',
            ProducerID: 'integer',
            ClubOwner: 'bool',
            RadioLicensesDate: 'date',
            RadioLicensesEndDate: 'date',
            SrialNum: 'string',
            SeatCount: 'integer',
            TakeoffCountBetweenTest: 'integer',
            DayCountBetweenTest: 'integer',
            HourCountBetweenTest: 'integer',
            GM_StartsTotal: 'integer',
            GM_StartsInspection: 'integer',
            GM_StartsToWarnBefore: 'integer',
            GM_HoursTotal: 'integer',
            GM_HoursInspection: 'integer',
            GM_HoursToWarnBefore: 'integer',
            GM_CofAinpectionDate: 'date',
            GM_CofAdaysToWarnBefore: 'integer',
            ZR_InitStarts: 'integer',
            ZR_InitHours: 'float',
            GM_AnnualInspectionDate: 'date',
            GM_AnnualdaysToWarnBefore: 'integer',
        }
    },
    {
        name: 'AircraftProducer',
        table: 'ctProducerID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string',

        }
    },
    {
        name: 'AircraftModel',
        table: 'ctModelID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string',

        }
    },

    {
        name: 'License',
        table: 'st01Licenses',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                },
                licenseType: {
                    field: 'LicensesTypeID',
                    model: 'LicenseType'
                }
            }
        },
        fields: {
            MemFKCode: 'integer',
            LicensesNum: 'integer',
            Expiration: 'date',
            LicensesTypeID: 'integer',
            ThisPKCode: 'integer',
        }
    },
    {
        name: 'LicenseType',
        table: 'ctLicensesTypeID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },

    {
        name: 'GliderOwnership',
        table: 'st03Gowner',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                glider: {
                    field: 'GFKCode',
                    model: 'Aircraft'
                },
                member: {
                    field: 'ownerNameIDSC',
                    model: 'Member'
                }
            }
        },
        fields: {
            GFKCode: 'integer',
            ownerNameIDSC: 'integer',
            BuyingDate: 'date',
            LeavingDate: 'date',
            OwnershipPercent: 'integer',
            ThisPKCode: 'integer',
        }
    },

    {
        name: 'Member',
        table: 't01Members',
        constraints: {
            pk: 'MemPKCode',
            // fks: {
            //     homeCity: {
            //         field: 'HomeCityID',
            //         model: 'HomeCity'
            //     }
            // }
        },
        fields: {
            MemPKCode: 'integer',
            FirstName: 'string',
            LastName: 'string',
            // HomeAddress: 'string',
            // HomeCityID: 'float',
            // HomeZip: 'integer',
            Email: 'string',
            EnLastName: 'string',
            EnFirstName: 'string',
            // HomePhone: 'string',
            // Notes: 'string',
            // WorkPhone: 'string',
            // AdministratorMember: 'bool',
            // DateJoined: 'date',
            // WorkFax: 'string',
            // FlyHourQty: 'integer',
            // Birthdate: 'date',
            // UserName: 'string',
            // CVV2: 'string',
            // GetPaymentOnJobsynID: 'integer',
            // GetPaymentOnDutyynID: 'integer',
            // MobilePhone: 'string',
            // Password: 'string',
            // TZ: 'string',
            // UserPermitionID: 'string',
            // CreditCardNumber: 'string',
            // CreditExpDate: 'string'
        }
    },
    {
        name: 'MemberStatus',
        table: 'stf01MemStatusID',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                },
                status: {
                    field: 'StatusID',
                    model: 'Status'
                }
            }
        },
        fields: {
            MemFKCode: 'integer',
            StatusID: 'integer',
            StatusDate: 'date',
            ThisPKCode: 'integer'
        }
    },
    {
        name: 'MemberDebitGroup',
        table: 'st01MemInDebitGroup',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                },
                debitGroup: {
                    field: 'DebitGroupIDSC',
                    model: 'DebitGroup'
                }
            }
        },
        fields: {
            MemFKCode: 'integer',
            DebitGroupIDSC: 'integer',
            paymentQty: 'integer',
            paymentRemaining: 'integer',
            NotActiveDebt: 'bool',
            ThisPKCode: 'integer'
        }

    },
    {
        name: 'Status',
        table: 'ctStatusID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'Medical',
        table: 'stf01Medical',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                }
            },
            fields: {
                MemFKCode: 'integer',
                Expiration: 'date',
                ThisPKCode: 'integer'
            }
        }
    },

    {
        name: 'PaymentType',
        table: 'ctPaymentTypeID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string',

        }
    },
    {
        name: 'PayersTypes',
        table: 'st08PayersTypes',
        constraints: {
            pk: 'PayersTypesCode',
            fks: {
                generalSetting: {
                    field: 'SettingFKCode',
                    model: 'GeneralSetting'
                }
            }
        },
        fields: {
            PayersTypesCode: 'integer',
            PayersType: 'string',
            PayersTypeParam: 'string',
            SettingFKCode: 'integer'
        }
    },
    {
        name: 'Price',
        table: 'st09PriceListsDetails',
        constraints: {
            pk: 'PricePKCode',
            fks: {
                priceList: {
                    field: 'PriceListsFKCode',
                    model: 'PriceList'
                }
            },
            fields: {
                PriceListFKCode: 'integer',
                PriceName: 'string',
                Price: 'integer',
                PricePKCode: 'integer'
            }
        }
    },
    {
        name: 'PriceList',
        table: 't09PriceLists',
        constraints: {
            pk: 'PriceListsPKCode'
        },
        fields: {
            PriceListsPKCode: 'integer',
            PriceListName: 'integer',
            PriceListStartDate: 'date',
            PriceListEndDate: 'date',
            MaxAmountForStay: 'integer'
        }
    },

    {
        name: 'Transact',
        table: 'st01Account',
        constraints: {
            pk: 'TransactID',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                }
            }
        },
        fields: {
            MemFKCode: 'integer',
            TransactID: 'integer',
            Amount: 'integer',
            PayQty: 'integer',
            Lastdate: 'date',
            statusIDSC: 'date',
        }
    },
    {
        name: 'DebitTransactLink',
        table: 'st04LinkPaymentsDebit',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                debit: {
                    field: 'DebitFKCode',
                    model: 'Debit'
                },
                transact: {
                    field: 'TransactID',
                    model: 'Transact'
                }
            }
        },
        fields: {
            DebitFKCode: 'integer',
            TransactID: 'integer',
            ThisPKCode: 'integer'
        }
    },
    {
        name: 'Debit',
        table: 'st06DebitDetails',
        constraints: {
            pk: 'DebitPKCode',
            fks: {
                debitOwner: {
                    field: 'MemNameIDSC',
                    model: 'Member'
                },
                debitBatch: {
                    field: 'DebitBatchFKCode',
                    model: 'DebitBatch'
                },
                debitSubject: {
                    field: 'DebitForID',
                    model: 'DebitSubject'
                }
            }
        },
        fields: {
            DebitPKCode: 'integer',
            MemNameIDSC: 'integer',
            DebitBatchFKCode: 'integer',
            DebitForID: 'integer',
            DebitDate: 'date',
            Amount: 'integer',
            DebitForText: 'string',
            Balance: 'integer'
        }
    },
    {
        name: 'DebitBatch',
        table: 't06DebitBatchs',
        constraints: {
            pk: 'DebitBatchPKCode'
        },
        fields: {
            DebitBatchPKCode: 'integer',
            BatchDescription: 'string',
            BatchDate: 'date',
            Done: 'bool',
            DoneAutoDebit: 'bool',
            AddToSum: 'bool'
        }
    },
    {
        name: 'DebitGroup',
        table: 'st08DebitGroupDefinition',
        constraints: {
            pk: 'DebitGroupPKCode',
            fks: {
                generalSetting: {
                    field: 'SettingFKCode',
                    model: 'GeneralSetting'
                },
                price: {
                    field: 'PriceNameIDSC',
                    model: 'Price'
                }
            },
            fields: {
                DebitGroupPKCode: 'integer',
                DebitGroupName: 'string',
                ShareDebit: 'bool',
                SettingFKCode: 'integer',
                PriceNameIDSC: 'integer'
            }
        }
    },
    {
        name: 'DebitSubject',
        table: 'ctDebitForID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'DebitDutyLink',
        table: 'st04LinkPayoffDetailDebit',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                debit: {
                    field: 'DebitFKCode',
                    model: 'Debit'
                },
                duty: {
                    field: 'DutyFKCode',
                    model: 'Duty'
                }
            }
        },
        fields: {
            ThisPKCode: 'integer',
            DebitFKCode: 'integer',
            DutyFKCode: 'integer'
        }
    },
    {
        name: 'Job',
        table: 'ctJobID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'JobType',
        table: 'ctJobTypeID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },

    {
        name: 'SwitchBoardItem',
        table: '[Switchboard Items]',
        constraints: {
            pk: 'SwitchboardID'
        },
        fields: {
            SwitchboardID: 'integer',
            ItemNumber: 'integer',
            ItemText: 'string',
            Command: 'integer',
            Argument: 'string'
        }
    },
    {
        name: 'HomeCity',
        table: 'ctHomeCityID',
        constraints: {
            pk: 'ID',
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'Supplier',
        table: 't05Suppliers',
        constraints: {
            pk: 'SPKCode',
            fks: {
                homeCity: {
                    field: 'HomeCityID',
                    model: 'HomeCity'
                },
                occupation: {
                    field: 'OccupationID',
                    model: 'Occupation'
                }
            },
            fields: {
                SPKCode: 'integer',
                Sname: 'string',
                Saddress: 'string',
                HomeCityID: 'integer',
                HomeZip: 'integer',
                Sphone: 'string',
                SmobilePhone: 'string',
                Fax: 'string',
                Email: 'string',
                Note: 'string',
                LicensedDealer: 'string',
                OccupationID: 'integer',
                Connect: 'string',
                ConnectPhone: 'string'
            }
        },
        fields: {
            DebitBatchPKCode: 'integer',
            BatchDescription: 'string',
            BatchDate: 'date',
            Done: 'bool',
            DoneAutoDebit: 'bool',
            AddToSum: 'bool'
        }
    },
    {
        name: 'LicenseType',
        table: 'ctLicensesTypeID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            type: 'string'
        }
    },

    {
        name: 'DegreeType',
        table: 'ctDegreeTypeID',
        constraints: {
            pk: 'ID'
        },
        fields: {
            ID: 'integer',
            Type: 'string'
        }
    },
    {
        name: 'Degree',
        table: 'st01degrees',
        constraints: {
            pk: 'ThisPKCode',
            fks: {
                member: {
                    field: 'MemFKCode',
                    model: 'Member'
                },
                degreeType: {
                    field: 'DegreeTypeID',
                    model: 'DegreeType'
                }
            },
        },
        fields: {
            MemFKCode: 'integer',
            DegreeNameID: 'integer',
            DegreeTypeID: 'integer',
            DegreeNote: 'string',
            TesterNameIDSC: 'integer',
            expiration: 'date',
            ThisPKCode: 'integer',
        }
    }
];


module.exports = tables;
