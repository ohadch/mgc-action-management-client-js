const opsTables = [
    {
        name: 'authorized_landing',
        fields: {
            pilot_first_name: 'text',
            pilot_last_name: 'text',
            pilot_phone_number: 'text',
            callsign: 'text',
            airplane_type: 'text',
            landing_date: 'date',
            authorizer_id: 'integer',
            landed: 'bool',
        }
    },
    {
        name: 'action_notes',
        fields: {
            action_id: "integer",
            content: "text"
        }
    },
    {
        name: 'flight_notes',
        fields: {
            flight_id: "integer",
            content: "text"
        }
    },
];

module.exports = opsTables;