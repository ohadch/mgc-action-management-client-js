let db = require('./config/db')[process.env.NODE_ENV];

module.exports = require('knex')({
    client: 'pg',
    connection: {
        host : db['host'],
        user : db['user'],
        password : db['password'],
        database : db['database']
    },
    useNullAsDefault: true
});