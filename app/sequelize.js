const env = require('dotenv').load();
const Sequelize = require('sequelize');
const context = require("../config/db");
const dbConfig = context[process.env.NODE_ENV];
const bcrypt = require('bcrypt-nodejs');
const UserModel = require('./models/user');
const jwt = require('jsonwebtoken');

console.log(`Sequelize DB Config: ${JSON.stringify(dbConfig)}`);

const sequelize = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const User = UserModel(sequelize, Sequelize);

User.prototype.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

User.prototype.generateJWT = function() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        email: this.email,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
};


User.prototype.toAuthJSON = function(){
    return {
        id: this.id,
        email: this.email,
        token: this.generateJWT(),
    };
};


async function createAdminUser() {
    let adminUser = await User.create({
        first_name: 'admin',
        last_name: 'admin',
        email: 'admin@example.com',
    });
    adminUser.update({password: adminUser.generateHash('+F]mqt+4Dy?)xrM9')});
    console.log("Admin user created.");
}

let force = false;
sequelize.sync({force:force})
    .then(async () => {
        console.log("Sequelize initialized!");
        if (force) {
            console.log(`Database & tables created!`);

            let admin = await User.findOne({ where: { email: 'admin@example.com' } });
            if (!admin) await createAdminUser();
        }
    });

module.exports = {
    User
};