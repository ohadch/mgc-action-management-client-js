"use strict";
const express = require('express');
const appController = require("../../controller/app-controller");
const AppRoutes = express.Router();


AppRoutes.get("/", appController.home);


module.exports = AppRoutes;