"use strict";
const _ = require("lodash");
const express = require('express');

const apiRoutes = express.Router();
const dbModel = require("../../const/db-model");
const pool = require("../../pool");
const {actionOf} = require("../../util/fetch");
const {SCHEMA} = require("../config");
const knex = require("../../knex-config");

const {
    itemsSuitableFor,
    getModelData,
    getFkModel,
    getModelByName,
    flightInStates,
    relevantOngoingFlights,
    flightFieldsOfModelList,
    assignedFks
} = require("../../util/fetch");

const {
    createFlight,
} = require('../../util/insert');

const {
    getPilotsFromListOfFlights,
    getTowPilotsFromListOfFlights,
    getGlidersFromListOfFlights,
    getTowAirplanesFromListOfFlights,
    calculateDailyStatsFromListOfFlights
} = require("../../util/summary");


function modelConstraints(modelName) {
    let candidates = dbModel.filter(model => {
        return model.name.toLowerCase() === modelName;
    });
    if (candidates.length === 0) {
        // If model was not found
        throw new Error(`Model '${modelName}' was not found.`);
    } else {
        // If model was found
        let model = candidates[0];
        return model.constraints;
    }
}


function modelFields(modelName) {
    let candidates = dbModel.filter(model => {
        return model.name.toLowerCase() === modelName;
    });
    if (candidates.length === 0) {
        // If model was not found
        throw new Error(`Model '${modelName}' was not found.`)
    } else {
        // If model was found
        let model = candidates[0];
        return model.fields;
    }
}

/*
Action related
 */


/**
 * Returns the action for a given date.
 * If no date is provided will return all of the actions.
 * Params: {
 *     dateStr: ISO Date string (e.g 2019-01-29)
 * }
 */
apiRoutes.get("/action", async (req, res) => {
    let {dateStr} = req.query;

    if (dateStr) {
        let date = new Date(dateStr.split("T")[0]);
        console.log(`Getting action of ${dateStr}`);
        let actionId = await actionOf(date);
        let actions = await getModelData('action');
        let action = actions.find(record => parseInt(record.__id) === parseInt(actionId));
        return res.json(action);
    } else {
        let modelData = await getModelData('action');
        return res.json(modelData);
    }

});

apiRoutes.get("/action/configuration", async (req, res) => {
    let {__id} = req.query;
    __id = parseInt(__id);

    if (isNaN(__id))
        res.status(500).json("Action ID must be a number");

    console.log(`Getting action configuration of action __id ${__id}`);
    let q = `
    SELECT *
    FROM ops.action_setup
    WHERE action_id = ${__id}
    `;

    let {rows} = await pool.query(q);
    return res.json(rows[0]);
});

apiRoutes.put("/action/configuration/:__id", async (req, res) => {
    let {__id} = req.params;
    let {config} = req.body;

    __id = parseInt(__id);

    if (isNaN(__id))
        res.status(500).json("Action ID must be a number");

    const SCHEMA = 'ops';

    let data = await knex('action_setup')
        .withSchema(SCHEMA)
        .where('action_id', '=', __id)
        .update(config)
        .returning('*');
    let flight = data[0];

    return res.json(flight);
});

apiRoutes.get("/action/notes", async (req, res) => {
    let {__id} = req.query;
    __id = parseInt(__id);

    if (isNaN(__id))
        res.status(500).json("Action ID must be a number");

    const SCHEMA = 'ops';

    let notes = await knex('action_notes')
        .withSchema(SCHEMA)
        .select()
        .where('action_id', __id);

    return res.json(notes);
});

apiRoutes.get("/flight/notes", async (req, res) => {
    let {__id} = req.query;
    __id = parseInt(__id);

    if (isNaN(__id))
        res.status(500).json("Flight ID must be a number");

    const SCHEMA = 'ops';

    let notes = await knex('flight_notes')
        .withSchema(SCHEMA)
        .select()
        .where('flight_id', __id);

    return res.json(notes);
});

apiRoutes.post("/action/notes", async (req, res) => {
    let {action_id, note} = req.body;

    let {content} = note;

    action_id = parseInt(action_id);

    if (isNaN(action_id))
        res.status(500).json("Action ID must be a number");

    const SCHEMA = 'ops';

    let data = await knex('action_notes')
        .withSchema(SCHEMA)
        .insert({action_id, content})
        .returning('*');

    let returningNote = data[0];

    return res.json(returningNote);
});

apiRoutes.post("/flight/notes", async (req, res) => {
    let {flight_id, note} = req.body;

    let {content} = note;

    flight_id = parseInt(flight_id);

    if (isNaN(flight_id))
        res.status(500).json("Flight ID must be a number");

    const SCHEMA = 'ops';

    let data = await knex('flight_notes')
        .withSchema(SCHEMA)
        .insert({flight_id, content})
        .returning('*');

    let returningNote = data[0];

    return res.json(returningNote);
});


/*
Model Related
 */

/**
 * Returns the data of a model by its name.
 */
apiRoutes.get("/model/data", async (req, res) => {
    let { modelName } = req.query;
    let modelData = await getModelData(modelName);
    return res.json(modelData);
});


/**
 * Returns the constraints of a model by its name.
 */
apiRoutes.get("/model/constraints", (req, res) => {
    let {modelName} = req.query;
    console.log(`Getting ${modelName} constraints`);
    let constraints = modelConstraints(modelName);
    return res.json(constraints);
});


/**
 * Returns the fields of a model by its name.
 */
apiRoutes.get("/model/fields", (req, res) => {
    let {modelName} = req.query;
    console.log(`Getting ${modelName} fields`);
    let fields = modelFields(modelName);
    return res.json(fields);
});


/**
 * Returns the model of a flight entry certain foreign key name.
 */
apiRoutes.get("/model/byFlightFKName", (req, res) => {
    let { fkName } = req.query;
    console.log(`Getting model of ${fkName}`);
    let model = getFkModel('flight', fkName);
    res.json(model);
});


/**
 * Returns the data that is suitable for a particular flight foreign key.
 */
apiRoutes.get("/suitableFor", async (req, res) => {
    let {fkName} = req.query;
    console.log(`Getting ${fkName} model data`);
    let { records } = await itemsSuitableFor(fkName);
    return res.json(records);
});

/**
 * Gets the available instances of a particular flight FK.
 * Available instances are instances that are not assigned
 * to any flight that is in the tow/ongoing stages.
 * @query_params: { fkName }
 */
apiRoutes.get("/available", async (req, res) => {
    const {actionId, fkName, available} = req.query;
    let availableOnly = available == "true";
    // Validate input
    if (![actionId, fkName].every(x => x))
        throw new Error("Please provide actionId and fkName (of flight) as querystring");
    console.log(`Getting available ${fkName}s on action ID ${actionId}`);
    let flightFks = getModelByName('flight').constraints.fks;
    let data = await itemsSuitableFor(fkName);
    if (availableOnly) {
        if (data.fkModelName.toLowerCase().includes(['flighttype'])) {
            return res.json({pk: getFkModel('flight', fkName).constraints.pk, records: data.records});
        } else {
            let flightFieldsOfModel = flightFieldsOfModelList(data, flightFks);
            let ongoingFlights = await relevantOngoingFlights(fkName, actionId);
            // Determine unavailable pks (pks that Rare assigned to tow/ongoing stage fight)
            let unavailablePks = assignedFks(ongoingFlights, flightFieldsOfModel);
            // At last filter out the unavailable pks from the possible instances
            let availableInstances = data.records.filter(record => {
                return !unavailablePks.includes(record[data.pk.toLowerCase()])
            });
            let result = {pk: getFkModel('flight', fkName).constraints.pk, records: availableInstances};
            return res.json(result);
        }
    } else {
        return res.json(data);
    }
});


/*
Summary Related
 */

/**
 * Returns a list of objects from a list of flights by category.
 * @return: [{name: 'a', id: '1'}, ]
 */
apiRoutes.post("/summary/values", async (req, res) => {
    console.log("Getting custom filter values");
    let category = req.body['category'];
    let flights = await flightInStates(['tow', 'ongoing', 'landed']);
    let values;
    if (category === 'Glider Pilot') {
        values = await getPilotsFromListOfFlights(flights);
    } else if (category === 'Tow Pilot') {
        values = await getTowPilotsFromListOfFlights(flights);
    } else if (category === 'Glider') {
        values = await getGlidersFromListOfFlights(flights);
    } else if (category === 'Tow Airplane') {
        values = await getTowAirplanesFromListOfFlights(flights);
    } else {
        throw new Error(`Unrecognized category: ${category}`);
    }
    res.json(values);
});


/**
 * Calculates the daily stats from a list of flights.
 * @body: {
 *     "category": <the category name (glider, tow pilot, )>,
 *     "value": <the object PK>,
 * }
 */
apiRoutes.post("/summary/stats", async (req, res) => {
    console.log("Getting daily summary stats");
    let category = req.body['category'];
    let value = parseInt(req.body['value']);
    let flights = await flightInStates(['tow', 'ongoing', 'landed']);
    if (category === 'Glider Pilot') {
        flights = flights.filter(f => [f['pilot1nameidsc'], f['pilot2nameidsc']].includes(value))
    } else if (category === 'Tow Pilot') {
        flights = flights.filter(f => f['dragpilotidsc'] === value)
    } else if (category === 'Glider') {
        flights = flights.filter(f => f['radiocallidsc'] === value)
    } else if (category === 'Tow Airplane') {
        flights = flights.filter(f => f['dragplaneradiocallidsc'] === value)
    } else {
        throw new Error(`Unrecognized category: ${category}`);
    }
    let stats = calculateDailyStatsFromListOfFlights(flights);
    res.json(stats);
});


/*
Flight related
 */


/**
 * Returns a list of the flights.
 */
apiRoutes.get("/flight/:__id?", async (req, res) => {
    // Extract filters
    let {__id} = req.params;
    let {actionId} = req.query;
    let flights;
    // Query
    if (actionId) {
        // If action ID is provided, return the flights of the current action
        console.log("Getting flights of action ID ");
        flights = await flightInStates(['tow', 'ongoing', 'landed'], actionId);
        return res.json(flights);
    }
    // Otherwise, return all the flights or only one if __id is provided
    flights = await getModelData('flight');
    if (__id) {
        let flight = flights.find(flight => flight.__id === parseInt(__id));
        if (!flight) {
            return res.status(204).json({})
        } else {
            return res.status(200).json(flight)
        }
    } else {
        return res.json(flights);
    }
});


/**
 * Creates a new flight.
 * @body: {
 *     Pilot1NameIDSC
 *     Pilot2NameIDSC
 *     DragPilotIDSC
 *     DragPlaneRadioCallIDSC
 *     RadioCallIDSC
 *     FlightTypeCode
 *     __action_id
 * }
 */
apiRoutes.post("/flight", async (req, res) => {
    console.log("Creating new flight");
    // Parse body
    const {
        Pilot1NameIDSC,
        Pilot2NameIDSC,
        DragPilotIDSC,
        DragPlaneRadioCallIDSC,
        RadioCallIDSC,
        FlightTypeIDSC,
        TakeOffTime,
        __state,
        __action_id
    } = req.body;
    // Insert the flight
    await createFlight({
        Pilot1NameIDSC,
        Pilot2NameIDSC,
        DragPilotIDSC,
        DragPlaneRadioCallIDSC,
        RadioCallIDSC,
        FlightTypeIDSC,
        TakeOffTime,
        __state,
        __action_id
    });
    res.json({"message": "success"});
});


/**
 * Updates a flight object in the database.
 * @body: {
 *     "__state": <state_name>,
 *     "dragtypeidsc": <tow_type_pk>
 * }
 */
apiRoutes.put("/flight", async (req, res) => {
    let { __id, __state, landtime, dragtypeidsc } = req.body;
    console.log(`Updating flight __id ${__id}`);
    // query
    let data = await knex('st04actionsdetails')
        .withSchema(SCHEMA)
        .where('__id', '=', __id )
        .update({ __state, dragtypeidsc, landtime: landtime ? landtime : null })
        .returning('*');
    let flight = data[0];
    return res.json(flight);
});


apiRoutes.put("/flight/edit/:flightId", async (req, res) => {
    let {flightId} = req.params;
    let {
        pilot1nameidsc,
        pilot2nameidsc,
        dragpilotidsc,
        dragplaneradiocallidsc,
        radiocallidsc,
        flighttypeidsc,
        takeofftime
    } = req.body;
    console.log(`Updating flight __id ${flightId}`);
    // query
    let data = await knex('st04actionsdetails')
        .withSchema(SCHEMA)
        .where('__id', '=', flightId)
        .update({
            pilot1nameidsc,
            pilot2nameidsc,
            dragpilotidsc,
            dragplaneradiocallidsc,
            radiocallidsc,
            flighttypeidsc,
            takeofftime
        })
        .returning('*');
    let flight = data[0];
    return res.json(flight);
});


/*
Authorizer related
 */

apiRoutes.post("/landing/new", async (req, res) => {
    console.log(`Creating new authorized landing`);

    let tableName = 'authorized_landing';

    let params = req.body;
    params.landed = false;
    params.authorizer_id = req.user.id;

    let data = await knex(tableName).insert(params).returning('*');
    res.json(data);
});


apiRoutes.post("/landing/land", async (req, res) => {
    console.log(`Setting landing as landed`);

    let q = `UPDATE authorized_landing
             SET landed = ${req.body.newState}
             WHERE __id = ${req.body.id}`;

    await pool.query(q);
    return res.json({"message": "success"});
});


apiRoutes.get("/authorizer/landings/data", async (req, res) => {
    console.log(`Getting authorized landings`);

    let q = `SELECT * FROM authorized_landing`;
    let data = await pool.query(q);

    res.json(data.rows);
});


module.exports = apiRoutes;
