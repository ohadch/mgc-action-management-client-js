const express = require('express');
const {getKnownEmails} = require('../../util/fetch');
const {User} = require('../sequelize');
const validation = require('../../services/req-validation');
const authController = require("../../controller/auth-controller");
const passport = require("passport");

const authRoutes = express.Router();

authRoutes.get("/login", authController.login);

authRoutes.get("/signup", (req, res) => {
    // res.sendFile(path.join(__dirname, "../../views", 'signup.html'));
    res.render('signup', { layout: 'auth' });
});

authRoutes.post("/login", passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

authRoutes.post("/logout", (req, res, next) => {
    req.logout();
    res.redirect('/')
});

authRoutes.post("/signup", validation.signUp, async (req, res) => {
    try {
        let emails = await getKnownEmails();

        // Further validations
        if (!emails.includes(req.body.email)) {
            throw new Error(`Email ${req.body.email} is unrecognized`)
        } else if (req.body.password !== req.body.confirm_password) {
            throw new Error(`Password fields do not match`)
        }

        // Create user
        let userConfig = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
        };
        console.log(`Creating user ${JSON.stringify(userConfig)}`);
        let user = await User.create(userConfig);

        await user.update({password: user.generateHash(req.body.password)});
        let authJson = user.toAuthJSON();

        req.login(authJson['id'], (err) => {
            if (err) throw err;
            return res.redirect('/')
        });

    } catch (e) {
        res.status(500).json({error: e.message})
    }

});

module.exports = authRoutes;