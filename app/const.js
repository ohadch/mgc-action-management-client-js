module.exports.LICENSE_TYPES = {
    glider: 1,
    airplane: 2,
    cfig: 3,
    student: 4,
    lsa: 5,
    inactive: 8
};