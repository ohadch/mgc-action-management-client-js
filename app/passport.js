const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const {User} = require('./sequelize');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    async function (email, password, done) {
        console.log(`Passport: user ${email}, password ${password}`);
        let user = await User.findOne({ where: {email: email} });

        if (user) {
            if (user.validPassword(password)) {
                return done(null, user);
            }
        }

        return done(null, false, {
            message: 'Please verify your credentials.'
        });

    }
));

passport.serializeUser((userId, done) => {
    done(null, userId);
});

passport.deserializeUser((userId, done) => {
    done(null, userId);
});

module.exports = passport;