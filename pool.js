require('dotenv').config();
const { Pool, Client } = require('pg');

const context = require("./config/db");
const dbConfig = context[process.env.NODE_ENV];

console.log("Env: " + process.env.NODE_ENV);
console.log(`pg DB Config: ${JSON.stringify(dbConfig)}`);

const pool = new Pool(dbConfig);
const opsSchema = 'ops';
const opsTables = require("./const/ops-tables-model");

// Create all executive tables
opsTables.forEach(async table => {
    await createTable(table);
    return true;
});

async function createTable(table) {
    console.log(`Creating table ${opsSchema}.${table.name}`);

    // Get the fields of the model table for the table creation query
    let fields = Object.keys(table.fields).map(key => {
        let dtype = table.fields[key];
        let keyString = `${key} ${dtype}`;
        return keyString;
    });
    fields.push("__id SERIAL PRIMARY KEY");
    let fieldsStr = fields.join(",");

    // Create the table in the db
    let q = `CREATE TABLE IF NOT EXISTS ${opsSchema}.${table.name} (${fieldsStr})`;
    await pool.query(q);
    return true;
}



module.exports = pool;