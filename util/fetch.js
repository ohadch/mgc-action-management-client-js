"use strict";

const {buildFromClause, buildSelectClause} = require("./sql");

const pool = require("../pool");
const unflatten = require('unflatten');
const schema = 'public';
const {LICENSE_TYPES} = require("../app/const");
const dbModel = require("../const/db-model");
const _ = require("lodash");

// These are functions that perform sql queries

async function getJoins(model) {
    let schemaTables = await getSchemaTables(schema);
    let joins;
    if (model.constraints.fks) {
        joins = [];
        Object.keys(model.constraints.fks).forEach(fk => {
            let foreignModel = getFkModel(model.name, fk);
            if (schemaTables.includes(foreignModel.table.toLowerCase())) {
                joins.push({
                    tableName: foreignModel.table.toLowerCase(),
                    localField: model.constraints.fks[fk].field.toLowerCase(),
                    foreignField: foreignModel.constraints.pk.toLowerCase(),
                    alias: fk
                })
            }
        });
    } else {
        joins = null;
    }
    return joins;
}


/**
 * Returns a list of the tables under the specified schema
 * @param schema {string} the schema name
 * @return {Promise<*>} A list of table names
 */
async function getSchemaTables(schema) {
    let q = `
     SELECT tablename
     FROM pg_catalog.pg_tables
     WHERE schemaname = '${schema}'
    `;
    let {rows} = await pool.query(q);
    let tables = rows.map(row => {
        return row['tablename'];
    });
    return tables;
}


/**
 * Returns a list of the members that hold one of the provided licenses
 * @param licenseTypesNamesList: A list of license type names
 * @return {Promise<*>} A list of member IDs
 */
async function getMembersWithLicenseType(licenseTypesNamesList) {
    let licenseTypesStr = licenseTypesNamesList.map(name => LICENSE_TYPES[name].toString()).join(",");
    let q = `SELECT member.mempkcode
     FROM st01licenses license
         LEFT JOIN ctlicensestypeid license_type
             ON license.licensestypeid = license_type.id
         LEFT JOIN t01members member
             on license.memfkcode = member.mempkcode
         WHERE license_type.id IN (${licenseTypesStr})
             AND license_type.id NOT IN (8)`;
    let {rows} = await pool.query(q);
    let memberIds = rows.map(row => row['mempkcode']);
    return memberIds;
}


/**
 * Returns a string representation of the model.
 * @param modelName {string} the model name
 * @param object {Object} the model record
 * @returns {string|*|string}
 */
function toString(modelName, object) {
    let str;
    if (modelName === 'member') {
        let { firstname, lastname } = object;
        str = `${firstname} ${lastname}`;
    } else if (modelName === 'action') {
        let { actiondate } = object;
        str = `Action|${actiondate.toLocaleString()}`;
    } else if (modelName === 'flighttype') {
        let { flighttype } = object;
        str = flighttype;
    } else if (modelName === 'aircraft') {
        let { radiocall } = object;
        if (radiocall)
            str = "4X-" + (radiocall.split("-").length > 1 ? radiocall.split("-")[1] : radiocall);
    } else if (modelName === 'flight') {
        let {__action_id, takeofftime, __glider} = object;
        str = `Flight(${__action_id}, ${__glider.radiocall}, ${takeofftime.toISOString()})`
    } else if (modelName === 'towtype') {
        let {towingtype} = object;
        return towingtype;
    } else {
        throw new Error(`toString does not support model: ${modelName}`);
    }
    return str;
}


/**
 * Returns the existing data of a specified object in the database.
 * @param modelName {string} The model name
 * @return {Promise<*>}
 */
async function getModelData(modelName) {
    console.log(`Getting model data for model ${modelName}`);
    let model = getModelByName(modelName);
    let joins = await getJoins(model);
    // Create from close
    let {fromClause, tableAlias} = buildFromClause(model, joins);
    // Create a select clause that is compatible with the joins
    let selectClause = buildSelectClause(model, joins, tableAlias);
    // Build the query
    let q = `${selectClause} ${fromClause}`;
    let result = await pool.query(q);
    // Unflatten the rows by prefix of "__"
    let rows = result.rows.map(row => unflatten(row, "___"));
    rows.forEach(row => { row.__str = toString(modelName, row) });
    return rows;
}


/**
 * Returns all the values that are FK of some flight.
 * @example: For fkName=pilot1, will return all the members that were once pilot1 of some flight.
 * @param fkName {string} The foreign key name.
 * @return {Promise<{records: *, fkModelName: *, pk: *}>}
 */
async function itemsSuitableFor(fkName) {
    console.log(`Getting flight fk values for fk ${fkName}`);
    let fkModel = getFkModel('flight', fkName);
    fkName = fkName.toLowerCase();
    let records = await getModelData(fkModel.name.toLowerCase());
    if (fkModel.name.toLowerCase() === 'aircraft') {
        if (fkName === 'dragplane') {
            records = records.filter(record => {
                return record.radiocall && !record.radiocall.startsWith("G") && !record.radiocall.startsWith("4X-G");
            })
        } else if (fkName === 'glider') {
            records = records.filter(record => {
                return record.radiocall && (record.radiocall.startsWith("G") || record.radiocall.startsWith("4X-G"));
            })
        }
    } else if (fkModel.name.toLowerCase() === 'member') {
        if (['pilot1', 'pilot2'].includes(fkName.toLowerCase())) {
            let gliderPilots = await getMembersWithLicenseType(['glider', 'cfig']);
            records = records.filter(record => {
                return record.mempkcode && gliderPilots.includes(record.mempkcode);
            })
        } else if (fkName === 'dragpilot') {
            let towPilots = await getMembersWithLicenseType(['airplane']);
            records = records.filter(record => {
                return record.mempkcode && towPilots.includes(record.mempkcode);
            })
        }
    }
    return {fkModelName: fkModel.name, pk: fkModel.constraints.pk, records: records};
}


/**
 * Creates new action row under t04actions.
 * @param actionDate: The action date
 * @return The created action ID
 */
async function createNewAction(actionDate) {
    let actionDateStr = stringifyDate(actionDate);
    let q = `INSERT INTO t04actions (actiondate) VALUES ('${actionDateStr}')`;
    let result = await pool.query(q);
    return result.rows.length && result.rows[0].__id;
}


/**
 * Returns the current time without timezone offset.
 * @return {Date}
 */
const currentTimeNoOffset = () => {
    let now = new Date();
    now = new Date(now - (now.getTimezoneOffset() * 60 * 1000));
    return now;
};


/**
 * Returns the current date without timezone offset.
 * @return {Date}
 */
const currentDateNoOffset = () => {
    let now = currentTimeNoOffset();
    let currentDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    return currentDate;
};


/**
 * A helper function to stringify date
 * @param date
 * @return {string}
 */
const stringifyDate = (date) => `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

async function deleteAction(actionId) {
    let q = `
        DELETE FROM t04actions 
        WHERE __id = ${actionId};
    `;
    await pool.query(q);
}


async function createActionSetupRecord(actionId) {
    let q = `SELECT * FROM ops.action_setup WHERE action_id = ${actionId}`;
    let {rows: actions} = await pool.query(q);
    if (!actions.length) {
        console.log("Creating action setup record for action __id: " + actionId);
        let qInsert = `
        INSERT INTO ops.action_setup (action_id) VALUES (${actionId})
        `;
        await pool.query(qInsert)
    }
}


async function actionOf(date) {
    let actionDateStr = stringifyDate(date);
    // Fetch the actions of the current date
    let q = `SELECT * FROM t04actions WHERE actiondate = '${actionDateStr}'`;
    let actions = await pool.query(q);
    actions = actions.rows;
    // Try to return the action, if no action exists create one and return it
    let actionId;
    if (actions.length > 1) {
        // If more than one action was found this is probably a bug
        console.log("Deleting redundant actions");
        await Promise.all(actions.slice(1).map(action => deleteAction(action.__id)));
        return actions[0].__id;
    } else if (actions.length === 1) {
        actionId = actions[0].__id;
    } else {
        actionId = await createNewAction(date);
    }
    await createActionSetupRecord(actionId);
    return actionId;
}


/**
 * Returns the current action.
 * There is only one action per day, so if no action is found,
 * the function creates the action.
 * @return {Promise<*>}
 */
async function getCurrentAction() {
    console.log("Getting current action");
    // Determine the action date
    let actionDate = currentDateNoOffset();
    return actionOf(actionDate);
}


/**
 * Returns a list of the flights in the specified states.
 * @param states: The list of the states. The available states: TOW, INFLIGHT, LANDED, CLOSED
 * @param actionId: The __id of the desired action
 * @return {Promise<void>}
 */
async function flightInStates(states, actionId) {
    console.log(`Getting flights in states: ${states.join(", ")}`);
    let flights = await getModelData('flight');
    // Filter flights that belong to the current actions and one of the specified states
    flights = flights.filter(flight => {
        return (flight.__action_id === actionId) && (states.includes(flight.__state));
    });
    return flights;
}


/**
 * Returns the list of the members in the MGC Access DB.
 * @return {Promise<*>}
 */
async function getKnownEmails() {
    console.log("Getting known emails");
    let q = 'SELECT email FROM t01members';
    let res = await pool.query(q);
    // For each member returns the email and drops null emails
    let emails = res.rows.map(row => row.email).filter(x => x);
    return emails;
}

/**
 * Returns only the relevant ongoing flights for the flight FK.
 * Explanation: only TOW state flights block tow_pilot and tow_airplane. On the contrary,
 *                  TOW and INFLIGHT state flights block glider and glider_pilot.
 * @param fkName: {string} The flight fk name
 * @return {*} A list of the relevant ongoing flights.
 */
async function relevantOngoingFlights(fkName, actionId) {
    let ongoingFlights;
    if (['dragplane', 'dragpilot'].includes(fkName.toLowerCase())) {
        ongoingFlights = await flightInStates(['tow'], actionId);
    } else {
        ongoingFlights = await flightInStates(['tow', 'ongoing'], actionId);
    }
    return ongoingFlights
}


/**
 * Returns
 * @param modelName
 * @return {*}
 */
function getModelByName(modelName) {
    console.log(`Getting model '${modelName}'`);
    let candidates = dbModel.filter(model => {
        return model.name.toLowerCase() === modelName.toLowerCase();
    });
    if (candidates.length > 1) {
        throw new Error(`Too many candidatmemes were found (${candidates.length}): '${modelName}'`);
    } else if (candidates.length === 1) {
        return candidates[0];
    } else {
        throw new Error(`Model was not found: '${modelName}'`);
    }
}


/**
 * Gets the model of an FK of a specified model.
 * @example: For modelName=Flight and fkName=pilot1 will return Member model.
 * @param modelName {string} The model name
 * @param fkName {string} The model's FK name
 * @return {*}
 */
function getFkModel(modelName, fkName) {
    console.log(`Getting model for fk '${fkName}' of model '${modelName}'`);
    let fkModelName = getModelByName(modelName).constraints.fks[fkName].model;
    let fkModel = getModelByName(fkModelName);
    return fkModel;
}


/**
 * List all the fields of the Flight model,
 * that are FKs that their primary table is
 * of the given model.
 * @example:
 * 'Aircraft' => radiocallidsc, dragplaneradiocallidsc
 *
 * @param data: {
 *     fkModelName
 * }
 * @param flightFks
 * @return {string[]}
 */
function flightFieldsOfModelList(data, flightFks) {
    let res = Object.keys(flightFks).filter(fk => {
        return flightFks[fk].model.toLowerCase() === data.fkModelName.toLowerCase();
    }).map(fk => {
        return flightFks[fk].field.toLowerCase();
    });
    return res;
}


/**
 * Determine unavailable pks (pks that are assigned to tow/ongoing stage fight)
 * @param ongoingFlights // TODO: fill docs
 * @param flightFieldsOfModel // TODO: fill docs
 */
function assignedFks(ongoingFlights, flightFieldsOfModel) {
    let unavailablePks = [];
    ongoingFlights.map(flight => {
        Object.keys(flight).forEach(key => {
            if (flightFieldsOfModel.includes(key.toLowerCase())) {
                unavailablePks.push(flight[key]);
            }
        })
    });
    return _.uniq(unavailablePks);
}


module.exports = {
    getModelData,
    getKnownEmails,
    itemsSuitableFor,
    flightInStates,
    relevantOngoingFlights,
    getModelByName,
    getFkModel,
    flightFieldsOfModelList,
    assignedFks,
    actionOf
};
