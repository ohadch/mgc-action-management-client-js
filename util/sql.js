"use strict";

const dbModel = require("../const/db-model");

// These are functions that build SQL queries
function typeCastQueryParams(model, params) {
    let paramsNormalized = {};
    params = Object.keys(params).forEach(field => {
        if (model.fields[field] === 'integer') {
            paramsNormalized[field] = parseInt(params[field]);
        } else if (field === '__id') {
            paramsNormalized[field] = parseInt(params[field]);
        } else if (field === '__state') {
            paramsNormalized[field] = `'${params[field]}'`;
        } else if (model.fields[field] === 'float') {
            paramsNormalized[field] = parseFloat(params[field]);
        } else if (['date', 'string', 'text'].includes(model.fields[field])) {
            paramsNormalized[field] = `'${params[field]}'`
        } else if (model.fields[field] === 'boolean') {
            paramsNormalized[field] = `${params[field].toUpperCase()}`
        } else {
            paramsNormalized[field] = params[field];
        }
    });
    return paramsNormalized;
}


/**
 * Builds SQL from clause that is compatible with Postgres.
 * @param model {Object} the queried model
 * @param joins {Array} an array of join objects
 * @return {object}
 */
function buildFromClause(model, joins) {
    let tableAlias = 'a';
    let fromClause;
    if (joins) {
        let abc = ['z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h', 'g', 'f', 'e', 'd', 'c', 'b'];
        joins.forEach(table => {
            table.variable = abc.pop();
        });
        let joinsClause = joins.map(join => {
            return `LEFT JOIN ${join.tableName} ${join.variable} ON ${tableAlias}.${join.localField} = ${join.variable}.${join.foreignField}`
        }).join('\n');
        fromClause = `FROM ${model.table} ${tableAlias} ${joinsClause}`;
    } else {
        fromClause = `FROM ${model.table} ${tableAlias}`;
    }
    return {fromClause, tableAlias};
}


/**
 * Builds SQL SELECT clause.
 * @param model {Object} the queried model
 * @param joins {Array} a list of join objects
 * @param tableAlias {string} the table alias
 * @return {string}
 */
function buildSelectClause(model, joins, tableAlias) {
    let selectClause;
    if (joins) {
        if (joins.length === 0) {
            joins = null;
        }
    }
    if (joins) {
        let joinedSelectedFieldsString = joins.map(join => {
            let tableFields = Object.keys(dbModel.find(model => model.table.toLowerCase() === join.tableName).fields);
            return tableFields.map(field => {
                return `${join.variable}.${field.toLowerCase()} AS __${join.alias}___${field.toLowerCase()}`
            }).join(", ");
        }).join(', ');
        selectClause = `SELECT ${tableAlias}.*, ${joinedSelectedFieldsString}`;
    } else {
        selectClause = `SELECT ${tableAlias}.*`;
    }
    return selectClause;
}



module.exports = {
    typeCastQueryParams,
    buildFromClause,
    buildSelectClause
};