"use strict";

const {lowerCaseObjectKeys} = require("./func");
const {SCHEMA} = require("../app/config");
const pool = require("../pool");


/**
 * Creates a new flight in the database.
 * @param Pilot1NameIDSC {number} The pilot1 ID
 * @param Pilot2NameIDSC {number} The pilot2 ID
 * @param DragPilotIDSC {number} The drag pilot ID
 * @param DragPlaneRadioCallIDSC {number} The drag plane ID
 * @param RadioCallIDSC {number} The glider ID
 * @param __action_id {number} The relevant action ID
 */
async function createFlight(newFlightConfig) {
    // Build and execute the query
    let q = require('knex')({client: 'pg'})('st04actionsdetails')
        .withSchema(SCHEMA)
        .insert(lowerCaseObjectKeys(newFlightConfig))
        .toString();
    return  await pool.query(q);
}

module.exports = {
    createFlight
};
