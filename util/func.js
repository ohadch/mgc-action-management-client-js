"use strict";

/**
 * Lowercases the keys of a given object.
 * @param obj
 */
module.exports.lowerCaseObjectKeys = (obj) => {
    let lowerCaseObj = {};
    Object.keys(obj).forEach(key => {
        lowerCaseObj[key.toLowerCase()] = obj[key]
    });
    return lowerCaseObj;
};

