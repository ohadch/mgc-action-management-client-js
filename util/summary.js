async function getPilotsFromListOfFlights(flights) {
    let ids = _.uniq([flights.map(f => f['pilot1nameidsc']), flights.map(f => f['pilot2nameidsc'])].filter(x => x));
    let members = await getModelData('member');
    members = members.filter(record => {
        return ids.includes(parseInt(record['mempkcode']));
    });
    let values = members.map(m => ({
        id: m.mempkcode,
        name: `${m.firstname} ${m.lastname}`,
        idField: 'mempkcode'
    }));
    return values;
}

async function getTowPilotsFromListOfFlights(flights) {
    let ids = _.uniq(flights.map(f => f['dragpilotidsc']));
    let members = await getModelData('member');
    members = members.filter(record => {
        return ids.includes(parseInt(record['mempkcode']));
    });
    let values = members.map(m => ({
        id: m.mempkcode,
        name: `${m.firstname} ${m.lastname}`,
        idField: 'mempkcode'
    }));
    return values;
}

async function getGlidersFromListOfFlights(flights) {
    let ids = _.uniq(flights.map(f => f['radiocallidsc']));
    let aircraft = await getModelData('aircraft');
    aircraft = aircraft.filter(record => {
        return ids.includes(parseInt(record['gpkcode']));
    });
    let values = aircraft.map(a => ({
        id: a.gpkcode,
        name: `${a['radiocall'].split("-")[a['radiocall'].split("-").length - 1]}`,
        idField: 'gpkcode'
    }));
    return values;
}

async function getTowAirplanesFromListOfFlights(flights) {
    let ids = _.uniq(flights.map(f => f['dragplaneradiocallidsc']));
    let aircraft = await getModelData('aircraft');
    aircraft = aircraft.filter(record => {
        return ids.includes(parseInt(record['gpkcode']));
    });
    let values = aircraft.map(a => ({
        id: a.gpkcode,
        name: `${a['radiocall'].split("-")[a['radiocall'].split("-").length - 1]}`,
        idField: 'gpkcode'
    }));
    return values;
}

function calculateDailyStatsFromListOfFlights(flights) {
    let landedFlights = flights.filter(f => f.landtime);
    let hasFlightOngoing = !!flights.find(f => !f.landtime);
    let flightTimeOfLandedFlightsSeconds = landedFlights.length ? landedFlights.map(f => (f['landtime'] - f['takeofftime']) / 1000).reduce((a, b) => a + b) : 0;
    return {
        landedFlights,
        hasFlightOngoing,
        flightTimeOfLandedFlightsSeconds
    }
}


module.exports = {
    getPilotsFromListOfFlights,
    getTowPilotsFromListOfFlights,
    getGlidersFromListOfFlights,
    getTowAirplanesFromListOfFlights,
    calculateDailyStatsFromListOfFlights
};