$("#newFlightButtonDiv").on("click", function () {
    $("#ongoingFlightsDiv").hide();
    $("#mainHeader").html("Dispatch New Flight");
    $("#newFlightView").removeClass("hidden").show();
});

$("#goBackFromNewFlightButton").on("click", function () {
    loadDashBoard();
});

$("#cancelNewFlightButton").on("click", function () {
    clearNewFlight();
    loadDashBoard();
});

$("#goToAuthorizeNewLandingButton").on("click", function () {
    $("#authorizedLandingsDiv").hide();
    $("#mainHeader").html("Authorize New Landing");
    $("#authorizeNewLandingDiv").removeClass("d-none").show();
});

$("#goToDashboardButton").on("click", function () {
    loadDashBoard();
});

$("#goToAuthorizerButton").on("click", function () {
    loadAuthorizer();
});

$("#goToSummaryButton").on("click", function () {
    loadSummary();
});

$("#cancelNewLandingButton").on("click", function () {
    $("#authorizeNewLandingDiv").hide();
    $("#authorizedLandingsDiv").removeClass("d-none").show();
    loadAuthorizer();
});

function loadDashBoard() {
    hideAll();
    renderFlights();
    renderOngoingAuthorizedLandings();
    $("#mainHeader").html("Ongoing Flights");
    $("#ongoingFlightsDiv").removeClass("d-none").show();
    setNavbarItemAsActive("#goToDashboardButton");
}

function loadSummary() {
    hideAll();
    // Populate the inputs
    populateCustomSummaryFilterTypes();
    // Render the summary tables
    renderSummaryTable();
    $("#mainHeader").html("Daily Summary");
    $("#dailySummaryView").removeClass("d-none").show();
    setNavbarItemAsActive("#goToSummaryButton");
}

function loadAuthorizer() {
    hideAll();
    renderAuthorizedLandings();
    $("#mainHeader").html("Landings Authorizer");
    $("#AuthorizerDiv").removeClass("d-none").show();
    setNavbarItemAsActive("#goToAuthorizerButton");
}

function hideAll() {
    $("#flightView").hide();
    $("#ongoingFlightsDiv").hide();
    $("#newFlightView").hide();
    $("#AuthorizerDiv").hide();
    $("#dailySummaryView").hide();
}


function setNavbarItemAsActive(navbarButtonId) {
    $(".nav-item").removeClass('active');
    $(navbarButtonId).parent().addClass("active");
}


// Main entry
loadDashBoard();