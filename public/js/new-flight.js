$.get("rest/flight/current-action", function (data) {
    $("#newFlightH").html("Flight #" + (data.length + 1));
});

modifyDispatchNewFlightButton();

// Set the selected value in the corresponding new flight form input
$(document).on("click", ".selection-item", function () {
    let fkName = this.id.split("-")[0];
    let fkStr = $(this).children()[0].innerHTML;
    let fkValue = this.id.split("-")[2];
    setNewFlightValue(fkName, fkStr, fkValue);
});


function getAllAvailableItems(fkName, fkModelName, unavailable) {
    // Gets all the available items
    getAvailable(fkName)
        .then((records) => {
            $(`#${fkName}sUl`).empty();
            records.forEach(record => {
                if (!unavailable.includes(record.pkFieldValue.toString())) {
                    $(`#${fkName}sUl`).append(itemTemplate(fkName, record.pkFieldValue, getStr(fkModelName, record)));
                }
            })
        });
}


$(".new-flight-input-view-value").on("click", function () {
    // Get all available items
    let fkName = $(this).parent().attr("data-fk-name");
    let fkModelName = $(this).parent().attr("data-fk-name-model");
    let unavailable = getPksAssignedToNewFlight(fkModelName);
    getAllAvailableItems(fkName, fkModelName, unavailable);

    // Handle removing assignment from attribute
    if ($(this).hasClass("new-flight-input-view-assigned")) {
        $(this)
            .removeClass("shadow-sm")
            .removeClass("new-flight-input-view-assigned")
            .html("Unassigned");
        removeNewFlightValue(fkName);
    }
    // If the attribute is not yet assigned,
    // hides all selection item uls,
    // and show only the relevant ul
    $(".new-flight-items-selection-list").hide();
    $(`#${fkName}sUl`).removeClass('hidden').show();
});

$(".new-flight-attr-input").change(function () {
    let fkName = $(this).attr("data-fk-name");
    let newValue = $(this).attr("data-val-str");
    if (newValue !== "") {
        $(`#${fkName}InputView`).children(".new-flight-input-view-value")
            .html(newValue)
            .addClass("shadow-sm")
            .addClass("new-flight-input-view-assigned");
    }
    modifyDispatchNewFlightButton();
});

$("#newFlightSelectionItemsFilterInput").on("keyup", function () {
    let searchText = $(this).val().toLowerCase();

    $('ul.new-flight-items-selection-list > li').each(function(){

        let currentLiText = $(this).text().toLowerCase(),
            showCurrentLi = currentLiText.indexOf(searchText) !== -1;

        $(this).toggle(showCurrentLi);

    });
});

$("#forceSetTakeOffTimeButton").on("click", function () {
    let now = new Date();
    $("#newFlightDispatchTime").val(now.toISOString());
    $(this).hide();
    $("#clearNewFlightTakeOffTimeButton").removeClass("d-none").show();
});

$("#clearNewFlightTakeOffTimeButton").on("click", function () {
    $("#newFlightDispatchTime").val("");
    $(this).hide();
    $("#forceSetTakeOffTimeButton").removeClass("d-none").show();
    modifyDispatchNewFlightButton();
});

function modifyDispatchNewFlightButton() {
    let disable = false;
    Array.from($(".new-flight-attr-input")).filter(input => {
        return input.id !== "pilot2Input";
    }).forEach(input => {
        if (input.value === "") disable = true;
    });
    if (disable) {
        $("#dispatchNewFlightButton")
            .addClass("disabled")
            .removeClass("btn-success")
            .addClass("btn-secondary")
            .off();

        if ($("#newFlightDispatchTime").val() === '') {
            $("#forceSetTakeOffTimeButton").show();
        } else {
            $("#clearNewFlightTakeOffTimeButton").show();
        }

    } else {
        $("#forceSetTakeOffTimeButton").hide();
        $("#dispatchNewFlightButton")
            .removeClass("disabled")
            .removeClass("btn-secondary")
            .addClass("btn-success")
            .on("click", function () {
            $.get("rest/flight/fields", function (res) {
                let newFormData = [];

                let formData = $("#newFlightForm").serializeArray();
                Object.keys(formData).forEach(key => {
                    let d = {};
                    d[key] = formData[key];
                    newFormData.push(d)
                });

                let takeOffTime = $("#newFlightDispatchTime").val();
                let now = new Date();
                takeOffTime = takeOffTime === '' ? $.format.date(now, 'yyyy-MM-dd HH:mm:ss') : takeOffTime;
                takeOffTime = `TIMESTAMP '${takeOffTime}'`;
                let action_id = $("#currentActionInput").val();

                formData.push({name: "takeofftime", value:takeOffTime});
                formData.push({name: "__action_id", value:parseInt(action_id)});

                $.ajax({
                    type: 'post',
                    url: "rest/flight/new",
                    data: formData,
                    dataType: 'json',
                    async: true,
                    success: dispatchFlight
                })
            })
        });
    }
}

function dispatchFlight() {
    // Clears new flight form
    clearNewFlight();
    loadDashBoard();
}

function getStr(modelName, data) {
    if (modelName === 'Member') {
        return data.firstname + " " + data.lastname;
    }
    else if (modelName === 'Aircraft') {
        return `${data.radiocall} - ${data.gname}`;
    }
    else if (modelName === 'FlightType') {
        return data.flighttype;
    }
}

function getAvailable(fkName) {
    return new Promise(
        (resolve => {
            $.get("rest/" + fkName + "/available", function (res) {
                let pkAssignedToNewFlight = $(`#${fkName}Input`).val();
                let records = res.records.filter(record => {
                    return record[res.pk] !== pkAssignedToNewFlight;
                }).map(record => {
                    record.pkFieldValue = record[res.pk.toLowerCase()];
                    return record;
                });
                resolve(records);
            })
        })
    )
}

function getPksAssignedToNewFlight(modelName) {
    let inputs = document.querySelectorAll(".new-flight-attr-input");
    let unavailablePks = Array.from(inputs).filter(input => {
        return $(input).attr("data-fk-model-name") === modelName;
    }).map(input => {
        return $(input).val();
    });
    return unavailablePks;
}

function itemTemplate(fkName, itemId, itemName) {
    return `<li id="${fkName}-selection-${itemId}" class="selection-item shadow-sm">
            <div>${itemName}</div>
    </li>`;
}

function setNewFlightValue(fkName, fkStr, fkValue) {
    console.log(fkName, fkValue);
    $(`#${fkName}Input`).attr("data-val-str", fkStr).val(fkValue).change();
}

function removeNewFlightValue(fkName) {
    $(`#${fkName}Input`).attr("data-val-str", "").val("").change();
}

function clearNewFlight() {
    $(".new-flight-input-view-value").each(function () {
        $(this)
            .removeClass("shadow-sm")
            .removeClass("new-flight-input-view-assigned")
            .html("Unassigned");
    });
    $(".new-flight-attr-input").each(function () {
        $(this).val('');
        $(this).attr('data-val-str', '');
    });
    $(".new-flight-items-selection-list").hide();
    modifyDispatchNewFlightButton();
}