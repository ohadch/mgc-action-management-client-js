function pilotName(pilot) {
    return `${strIfExists(capitalize(pilot.firstname))} ${strIfExists(capitalize(pilot.lastname))}`
}

function strIfExists(str) {
    return str ? str : '';
}


function durationStr(totalSeconds) {
    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    return [zfill(hours, 2), zfill(minutes, 2), zfill(seconds, 2)].join(':')
}

function flightDuration(flight) {
    if (flight.takeofftime && flight.landtime) {
        let flightDurationSeconds = (new Date(flight.landtime) - new Date(flight.takeofftime)) / 1000;
        return durationStr(flightDurationSeconds)
    } else {
        return '?'
    }
}


function createSummaryRow(flight) {
    return `<tr id="summary-row-${flight.__id}" class="flight-summary-row">
        <td></td>
        <td>${flight.__glider.radiocall.toUpperCase()}</td>
        <td>${flight.__glider.gname}</td>
        <td>${pilotName(flight.__pilot1)}</td>
        <td>${pilotName(flight.__pilot2)}</td>
        <td>${flight.__flighttype.flighttype}</td>
        <td>${$.format.date(new Date(flight.takeofftime), 'HH:mm:ss')}</td>
        <td>${flight.landtime ? $.format.date(new Date(flight.landtime), 'HH:mm:ss') : ''}</td>
        <td>${flightDuration(flight)}</td>
        <td>${flight.__dragplane.radiocall.toUpperCase()}</td>
        <td>${capitalize(flight.__dragpilot.firstname)} ${capitalize(flight.__dragpilot.lastname)}</td>
        <td>${strIfExists(getTowingType(flight))}</td>
    </tr>`
}


function renderSummaryTable() {
    return new Promise((resolve => {
        $.get('/rest/flight/current-action', function (flights) {
            $("#dailySummaryTableBody").empty();
            flights.forEach(flight => {
                $("#dailySummaryTableBody").append(createSummaryRow(flight));
                resolve(true);
            })
        });
    }))
}

$("#summaryTableFilterInput").keyup(filterSummaryTable);

function filterSummaryTable() {
    let value = $(this).val().toLowerCase();
    $("#dailySummaryTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}


function populateCustomSummaryFilterTypes() {
    $("#customSummaryTypeSelect").empty();
    ['Glider Pilot', 'Tow Pilot', 'Glider', 'Tow Airplane'].forEach(value => {
        $("#customSummaryTypeSelect").append(`<option>${value}</option>`)
    });
    fillCustomSummaryFilterValues();
}


function getCustomSummaryValuesByCategory(category) {
    return new Promise((resolve) => {
        $.ajax({
            type: 'post',
            url: "rest/summary/values",
            data: {
                'category': category,
            },
            dataType: 'json',
            success: function (values) {
                resolve(values)
            }
        })
    })

}


async function fillCustomSummaryFilterValues() {
    $("#customSummaryValueSelect").empty();
    let category = $("#customSummaryTypeSelect").val();
    let values = await getCustomSummaryValuesByCategory(category);
    values.forEach(value => {
        $("#customSummaryValueSelect").append(`<option id="${value.id}">${value.name}</option>`)
    });
    fetchDailySummaryForSelectedCustomFilters();
}


function fetchDailySummaryByFilters(category, value) {
    return new Promise((resolve) => {
        $.ajax({
            type: 'post',
            url: "rest/summary/stats",
            data: {
                'category': category,
                'value': value,
            },
            dataType: 'json',
            success: function (values) {
                resolve(values)
            }
        })
    })
}

async function fetchDailySummaryForSelectedCustomFilters() {
    let category = $("#customSummaryTypeSelect").val();
    let value = $("#customSummaryValueSelect").children(":selected").attr("id");
    let stats = await fetchDailySummaryByFilters(category, value);
    // Set the values
    $("#landedFlightsSummaryVal").html(stats['landedFlights'].length);
    $("#hasFlightOngoingSummaryVal").html(stats['hasFlightOngoing'] ? 'Yes' : 'No');
    $("#totalFlightTimeSummaryVal").html(durationStr(stats['flightTimeOfLandedFlightsSeconds']));

}


$("#customSummaryTypeSelect").change(fillCustomSummaryFilterValues);
$("#customSummaryValueSelect").change(fetchDailySummaryForSelectedCustomFilters);

