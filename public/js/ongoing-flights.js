let stateToIdMap = {
    'tow': "#towsList",
    'ongoing': "#flightsList",
    'landed': "#landingsList",
};

let idToStateMap = {
    "towsList": 'tow',
    "flightsList": 'ongoing',
    "landingsList": 'landed',
};

$( function() {
    $( "#landingsList" ).sortable({
        connectWith: "#flightsList",
        receive: handleStateChange
    }).disableSelection();
    $( "#flightsList" ).sortable({
        connectWith: ["#towsList", '#landingsList'],
        receive: handleStateChange
    }).disableSelection();
    $( "#towsList" ).sortable({
        connectWith: "#flightsList",
        receive: handleStateChange
    }).disableSelection();
    $("#ongoingAuthorizedLandingsUl").sortable();
} );

$(document).on('click', '.ongoing-authorized-landing-li', function () {
    let landingId = parseInt($(this).attr("data-landing-id"));
    let landed;
    $(this).remove();
    if ($(this).hasClass('authorized-landing-landed')) {
        $("#ongoingAuthorizedLandingsUl").prepend(this);
        $(this).removeClass('authorized-landing-landed');
        landed = false;
    } else {
        $("#ongoingAuthorizedLandingsUl").append(this);
        $(this).addClass('authorized-landing-landed');
        landed = true;
    }
    $.ajax({
        type: 'post',
        url: "rest/landing/land",
        data: {
            'id': landingId,
            'newState': landed
        },
        dataType: 'json'
    })
});

$(document).on("click", ".flight-item", function () {
    let flightId = $(this).attr("id").split("-")[1];
    loadFlightViewForFlight(flightId);
});

$(document).on("click", ".tow-type-li", function (event) {
    let flightId = $("#towTypeModalFlightIdInput").val();
    let towTypeId = this.id.split("-")[1];
    updateState(flightId, 'ongoing', towTypeId);
    $("#towTypeSelectionModal").modal("hide");
});

$("#closeTowTypesModalButton").on("click", function () {
    let flightId = $("#towTypeModalFlightIdInput").val();
    $("#towsList").prepend($("#flightLi-" + flightId));
    updateState(flightId, 'tow');
    $("#towTypeModalFlightIdInput").val('');
});

function requireTowHeight(flightId) {
    $("#towTypesUl").empty();
    $.get("/rest/towtype/data", function (towTypes) {
        towTypes.forEach(towType => {
            $("#towTypesUl").append(createTowTypeLi(towType))
        })
    });
    $("#towTypeModalFlightIdInput").val(flightId);
    $("#towTypeSelectionModal").modal("show");
}


function handleStateChange(event, ui) {
    let oldState = idToStateMap[ui.sender.attr("id")];
    let newState = idToStateMap[event.target.id];

    let flightId = ui.item.attr("id").split("-")[1];

    if ((oldState === 'tow') && (newState === 'ongoing')) {
        requireTowHeight(flightId);
    } else {
        updateState(flightId, newState);
    }
}


function zfill(num, size) {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}


function updateState(flightId, newState, towTypeId) {
    let data = [{'name': '__state', 'value': newState}];
    if (towTypeId) {
        data.push({'name': 'dragtypeidsc', 'value': parseInt(towTypeId)});
    }
    $.get("/rest/byId/flight/" + flightId, function (flight) {
        // Set airplane&glider header
        $("#ongoing-item-header-" + flightId).html(getOngoingItemHeader(flight, newState));
        let landtime;
        // Set landing time
        if (newState === 'landed') {
            landtime = new Date();
            data.push({'name': 'landtime', 'value': `TIMESTAMP '${$.format.date(landtime, 'yyyy-MM-dd HH:mm:ss')}'`});
        } else {
            landtime = null;
            data.push({'name': 'landtime', 'value': 'NULL'});
        }
        // Set time data
        if (landtime) {
            landtime.setHours(landtime.getHours() - (landtime.getTimezoneOffset() / 60));
        }
        flight.landtime = landtime ? landtime.toISOString().split(".")[0] + ".000Z" : landtime;
        $("#ongoing-item-times-" + flightId).html(flightTimeStr(flight));
        $.ajax({
            type: 'put',
            url: "rest/flight/update/" + flightId,
            data: data,
            dataType: 'json',
        });
    });
}


function createTowTypeLi(towType) {
    let li = `<li class="tow-type-li" id="towType-${towType.towingtypecode}">
                <button class="btn btn-lg btn-success">${towType.towingtype}</button>
              </li>`;
    return li;
}


function flightTimeToStr(flightTime) {
    let str;
    if (flightTime) {
        let flightDateObj;
        if (typeof flightTime === 'string') {
            flightDateObj = new Date(flightTime);
        } else if (typeof  flightTime === 'object') {
            flightDateObj = flightTime;
        } else {
            throw new Error("Unexpected type of flight time: " + typeof flightTime);
        }
        flightDateObj = new Date($.format.date(flightDateObj, 'yyyy-MM-dd HH:mm:ss'));
        str = $.format.date(flightDateObj, 'HH:mm:ss');
    } else {
        str = "?";
    }
    return str;
}


function flightTimeStr(flight) {
    // Format as string
    let takeOffTimeStr = flightTimeToStr(flight['takeofftime']);
    let landingTimeStr = flightTimeToStr(flight['landtime']);
    return `${takeOffTimeStr}-${landingTimeStr}`;
}


function getFlightPilotNamesStr(flight) {
    let pilots = [flight.__pilot1, flight.__pilot2].map(pilot => {
        let name = getName(pilot);
        if (name !== "null null") {
            return `<p class="card-text">${getName(pilot)}</p>`
        }
    }).join("");
    return pilots;
}


function createFlightLi(flight) {
    // Generate values
    let header = getOngoingItemHeader(flight, flight.__state);
    let pilots = getFlightPilotNamesStr(flight);
    let flightTime = flightTimeStr(flight);
    // Create Lis
    let li = `<li id="flightLi-${flight.__id}" class="flight-item shadow-sm p-2 mb-3 bg-white">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title" id="ongoing-item-header-${flight.__id}">${header}</h5>
            ${pilots}
            <div id="ongoing-item-times-${flight.__id}">${flightTime}</div>
          </div>
        </div>
    </li>`;
    return li;
}


function getOngoingItemHeader(flight, state) {
    let headerStr = getCallsign(flight.__glider);
    if (state.toLowerCase() === 'tow') headerStr += ` - ${getCallsign(flight.__dragplane)}`;
    return headerStr;
}


function renderFlights() {
    $(".state-list").empty();
    $.get("/rest/flight/current-action", function (flights) {
        flights.forEach(flight => {
            $(stateToIdMap[flight.__state]).append(createFlightLi(flight));
            let item = $(`#flightLi-${flight.id}`);
            item.attr("parent", item.parent().attr("id"));
        });
    });
}


function createSingleFlightTable(flight) {
    return `<tr>
            <td>
            <strong>Glider</strong>
            </td>
            <td>${getCallsign(flight.__glider)}</td>
        </tr>
        <tr>
            <td>
            <strong>Pilot 1</strong>
            </td>
            <td>${getName(flight.__pilot1)}</td>
        </tr>
        <tr>
            <td>
            <strong>Pilot 2</strong>
            </td>
            <td>${getName(flight.__pilot2)}</td>
        </tr>
        <tr>
            <td>
            <strong>TakeOff Time</strong>
            </td>
            <td>${flight.takeofftime}</td>
        </tr>
        <tr>
            <td>
            <strong>Landing Time</strong>
            </td>
            <td>${flight.landtime}</td>
        </tr>
        <tr>
            <td>
            <strong>Duration</strong>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
            <strong>Tow Airplane</strong>
            </td>
            <td>${getCallsign(flight.__dragplane)}</td>
        </tr>
        <tr>
            <td>
            <strong>Tow Pilot</strong>
            </td>
            <td>${getName(flight.__dragpilot)}</td>
        </tr>
        <tr>
            <td>
            <strong>State</strong>
            </td>
            <td>${capitalize(flight.__state)}</td>
        </tr>
        <tr>
            <td>
            <strong>Flight Type</strong>
            </td>
            <td>${capitalize(flight.__flighttype.flighttype)}</td>
        </tr>
        <tr>
            <td>
            <strong>Tow Type</strong>
            </td>
            <td>${getTowingType(flight)}</td>
        </tr>`
}


function loadFlightViewForFlight(flightId) {
    $("#ongoingFlightsDiv").hide();
    $("#viewSingleFlightTable").empty();
    $.get('/rest/flight/current-action', function (flights) {
        let flight = flights.find(flight => {
            return parseInt(flight.__id) === parseInt(flightId);
        });
        $("#viewSingleFlightTable").append(createSingleFlightTable(flight));
    });
    $("#flightView").removeClass("d-none").show();
}


function createOngoingLandingLi(landing) {
    let classes = ["ongoing-authorized-landing-li", "shadow-sm"];
    if (landing.landed) classes.push("authorized-landing-landed");
    return `<li data-landing-id="${landing.__id}" class="${classes.join(' ')}">${landing.callsign.toUpperCase()}</li>`;
}


function renderOngoingAuthorizedLandings() {
    $("#ongoingAuthorizedLandingsUl").empty();
    getTodaysAuthorizedLandings()
        .then((landings) => {
            landings.forEach(landing => {
                $("#ongoingAuthorizedLandingsUl").append(createOngoingLandingLi(landing));
            })
        })
}
