$(document).ajaxStart(function() {
    $(document.body).css({'cursor' : 'wait'});
}).ajaxStop(function() {
    $(document.body).css({'cursor' : 'default'});
});

$.get("/rest/action/current", function (action) {
    $("#currentActionInput").val(action.__id);
    let date = normalizeTimeZone(new Date(action.actiondate));
    $("#currentActionInput").attr("data-date", $.format.date(date, "yyyy-MM-dd"));
});

function normalizeTimeZone(date) {
    date = new Date(date);
    date = new Date(date - (date.getTimezoneOffset() * 60 * 1000));
    return date;
}

function getTodaysAuthorizedLandings() {
    return new Promise(
        (resolve => {
            $.get("/rest/authorizer/landings/data", function (landings) {
                landings = landings.filter(landing => {
                    let currentDate = $("#currentActionInput").attr("data-date");
                    let landingDate = normalizeTimeZone(new Date(landing.landing_date));
                    landingDate = $.format.date(landingDate, 'yyyy-MM-dd');
                    return  landingDate === currentDate;
                });
                resolve(landings);
            });
        })
    )
}

function getName(member) {
    return capitalize(member.firstname) + ' ' + capitalize(member.lastname);
}

function getCallsign(aircraft) {
    return aircraft.radiocall.replace('4X-', '')
}

function getTowingType(flight) {
    let towType = flight.__dragtype;
    if (towType) {
        return towType.towingtype;
    } else {
        return null;
    }
}