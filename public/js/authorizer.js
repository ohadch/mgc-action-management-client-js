modifyAuthorizeNewLandingButton();

$(".new-landing-authorization-input").change(modifyAuthorizeNewLandingButton);

function modifyAuthorizeNewLandingButton() {
    let disable = false;
    Array.from($(".new-landing-authorization-input")).forEach(input => {
        if (input.value === "") disable = true;
    });
    if (disable) {
        $("#authorizeNewLandingButton")
            .removeClass("btn-success")
            .addClass("btn-secondary")
            .addClass("disabled")
            .off();
    } else {
        $("#authorizeNewLandingButton")
            .removeClass("disabled")
            .removeClass("btn-secondary")
            .addClass("btn-success")
            .on("click", function () {
                let formData = $("#authorizeNewLandingForm").serializeArray();
                $.ajax({
                    type: 'post',
                    url: "rest/landing/new",
                    data: formData,
                    dataType: 'json',
                    async: true,
                    success: authorizeNewLanding
                })
        })
    }
}

function authorizeNewLanding() {
    $(".new-landing-authorization-input").val('');
    modifyAuthorizeNewLandingButton();
    loadDashBoard();
}

function createLandingRow(landing) {
    let date = new Date(landing.landing_date);
    return `<tr>
        <td>${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}</td>
        <td>${landing.callsign.toUpperCase()}</td>
        <td>${landing.airplane_type}</td>
        <td>${capitalizeFirstLetter(landing.pilot_first_name)} ${capitalizeFirstLetter(landing.pilot_last_name)}</td>
        <td>${landing.pilot_phone_number}</td>
        <td>${capitalize(landing.authorizer_name)}</td>
    </tr>`
}

function renderAuthorizedLandings() {
    $("#landingsTableBody").empty();
    getTodaysAuthorizedLandings()
        .then((landings) => {
            landings.forEach(landing => {
                $("#landingsTableBody").append(createLandingRow(landing));
            })
        })
}

function capitalize(string) {
    if (string) {
        return string.split(" ").map(word => capitalizeFirstLetter(word)).join(" ");
    } else {
        return string;
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}