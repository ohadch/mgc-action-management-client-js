const path = require('path');

function capitalize(s) {
    return s.split(" ").map(w => w.charAt(0).toUpperCase() + w.substr(1)).join(" ")
}

module.exports = {

    home: (req, res, next) => {
        // console.log(`Active user: ${req.user}, authenticated: ${req.isAuthenticated()}`);
        res.sendFile(path.join(__dirname, "../vue", "dist", 'index.html'));
        // let userRealName = capitalize(`${req.user.first_name} ${req.user.last_name}`);
        // res.render('base', {userRealName});
    },

};