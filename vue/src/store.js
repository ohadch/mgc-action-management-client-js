import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        action: localStorage.getItem('action') ? JSON.parse(localStorage.getItem('action')) : ''
    },
    mutations: {
        setAction(state, action) {
            localStorage.setItem('action', JSON.stringify(action));
            state.action = action
        }
    },
    actions: {
        async setAction({ commit }, action) {
            commit('setAction', action)
        },
        async exitAction({commit}) {
            commit('setAction', '')
        }
    },
    getters: {
        action: state => state.action,
        actionId: state => state.action.__id
    }
});