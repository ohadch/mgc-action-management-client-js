export function stringifyMember(object) {
    return object.firstname + " " + object.lastname
}

export function stringifyAircraft(object) {
    let {radiocall} = object;
    return radiocall.split("-").length > 1 ? radiocall.split("-")[1] : radiocall
}