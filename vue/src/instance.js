/* eslint-disable */

import axios from 'axios';

const instance = axios.create({
    baseURL: '/rest'
    // baseURL: 'http://localhost:8000/rest'
});

// before a request is made start the nprogress
instance.interceptors.request.use(config => {
    NProgress.start();
    return config;
});

// before a response is returned stop nprogress
instance.interceptors.response.use(response => {
    NProgress.done();
    return response;
});

export default instance;
