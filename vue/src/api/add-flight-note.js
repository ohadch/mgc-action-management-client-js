import instance from '../instance';

export default async function addFlightNote(flightId, content) {
    let {
        data: note
    } = await instance.post('/flight/notes', {
        flight_id: flightId,
        note: {
            content
        }
    });
    return note;
}