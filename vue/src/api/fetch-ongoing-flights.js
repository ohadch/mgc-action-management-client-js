import instance from "../instance";

export default async function fetchOngoingFlights(actionId) {
    let {
        data: flights
    } = await instance.get('/flight', {
        params: {
            actionId
        }
    });
    return flights;
}