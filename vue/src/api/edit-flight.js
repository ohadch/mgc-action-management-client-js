import instance from '../instance';

export default async function editFlight(flightId, {
    pilot1Id,
    pilot2Id,
    dragPilotId,
    gliderId,
    dragPlaneId,
    flightTypeId,
    takeOffTime
}) {
    let {
        data: pilots
    } = await instance.put('/flight/edit/' + flightId, {
        pilot1nameidsc: pilot1Id,
        pilot2nameidsc: pilot2Id,
        dragpilotidsc: dragPilotId,
        radiocallidsc: gliderId,
        dragplaneradiocallidsc: dragPlaneId,
        flighttypeidsc: flightTypeId,
        takeofftime: takeOffTime
    });
    return pilots;
}