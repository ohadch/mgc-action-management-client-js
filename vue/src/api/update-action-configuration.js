import instance from "../instance"

export default async function updateActionConfiguration({
                                                            __id,
                                                            instructor1,
                                                            instructor2,
                                                            responsible1,
                                                            responsible2,
                                                            towPilot1,
                                                            towPilot2
                                                        }) {
    let {
        data: actions
    } = await instance.put(`/action/configuration/${__id}`, {
        config: {
            instructor1,
            instructor2,
            responsible1,
            responsible2,
            towPilot1,
            towPilot2,
        }
    });
    return actions;
}