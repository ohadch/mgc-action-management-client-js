import instance from "../instance"

export default async function listActions() {
    let {
        data: actions
    } = await instance.get("/action");
    return actions;
}