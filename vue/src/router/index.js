import Vue from 'vue';
import Router from 'vue-router';
import NewFlight from '@/components/NewFlight';
import EditFlight from '@/components/EditFlight';
import ActionSetup from '@/components/ActionSetup';
import OngoingFlightsBoardDataProvider from '@/components/OngoingFlightsBoardDataProvider';
import Summary from '@/components/summary/Summary';
import Notes from '@/components/notes/Notes';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/action',
      component: ActionSetup
    },
    {
      path: '/flight/new',
      component: NewFlight
    },
    {
      path: '/flight/edit/:flightId',
      component: EditFlight
    },
    {
      path: '/ongoing',
      component: OngoingFlightsBoardDataProvider
    },
    {
      path: '/summary',
      component: Summary
    },
    {
      path: '/notes',
      component: Notes
    },
    {
      path: '*',
      redirect: '/ongoing'
    }
  ],
});
